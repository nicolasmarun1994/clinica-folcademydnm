package com.folcademy.clinica.Exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice

public class GlobalExceptionHandler {
    @ResponseBody
    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<ErrorMesagge> defaultErrorHandler(HttpServletRequest req, Exception e){
        return new ResponseEntity<ErrorMesagge>(new ErrorMesagge("Error Generico", e.getMessage(), "1", req.getRequestURI()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ResponseBody
    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<ErrorMesagge> notFoundHandler(HttpServletRequest req, Exception e){
        return new ResponseEntity<ErrorMesagge>(new ErrorMesagge("Not Found", e.getMessage(), "2", req.getRequestURI()), HttpStatus.NOT_FOUND);
    }

    @ResponseBody
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<ErrorMesagge> badRequestException(HttpServletRequest req, Exception e){
        return new ResponseEntity<ErrorMesagge>(new ErrorMesagge("Bad request", e.getMessage(), "3", req.getRequestURI()), HttpStatus.NOT_FOUND);
    }
}
