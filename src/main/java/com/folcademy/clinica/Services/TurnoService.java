
package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Exceptions.NotFoundException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Model.Mappers.TurnoMapper;
import com.folcademy.clinica.Model.Repositories.TurnoRepository;
import com.folcademy.clinica.Services.Interfaces.ITurnoService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


@Service("TurnoService")
public class TurnoService implements ITurnoService {
    private final TurnoRepository turnoRepository;
    private final TurnoMapper turnoMapper;

    public TurnoService(TurnoRepository turnoRepository, TurnoMapper turnoMapper)
    {
        this.turnoRepository = turnoRepository;
        this.turnoMapper = turnoMapper;
    }

     @Override
    public List<Turno> findAllTurnos() {
        List<Turno> turnos = (List<Turno>) turnoRepository.findAll();
         return turnos;
     }
/*
    public List<TurnoEnteroDto> listarTodos(){
        return turnoRepository.findAll().stream().map(turnoMapper::entityToEnteroDto).collect(Collectors.toList());
    }
*/
    public TurnoEnteroDto listarUno(Integer id){
        if (!turnoRepository.existsById(id))
            throw new RuntimeException("No existe el paciente");
        return turnoRepository.findById(id).map(turnoMapper::entityToEnteroDto).orElse(null);
    }

    public TurnoEnteroDto agregar(TurnoEnteroDto entity){
        entity.setId(null);
       /* if (entity.getFecha()==null)
            throw new NotFoundException("Error ingrese una fecha");
        if (entity.getHora()==null)
            throw new BadRequestException("Error ingrese una hora");*/
        if (entity.getIdpaciente()<=0)
            throw new NotFoundException("Error ingrese un paciente correcto");
        if (entity.getIdmedico()<=0)
            throw new NotFoundException("Error ingrese un medico correcto");
        return turnoMapper.entityToEnteroDto(turnoRepository.save(turnoMapper.enteroDtoToEntity(entity)));
    }

    public TurnoEnteroDto editar(Integer idTurno, TurnoEnteroDto dto){
        if(!turnoRepository.existsById(idTurno))
            throw  new RuntimeException("No existe el turno");
        if (dto.getFecha()==null)
            throw new NotFoundException("Error ingrese una fecha");
        if (dto.getHora()==null)
            throw new BadRequestException("Error ingrese una hora");
        if (dto.getIdpaciente()<=0)
            throw new NotFoundException("Error ingrese un paciente correcto");
        if (dto.getIdmedico()<=0)
            throw new NotFoundException("Error ingrese un medico correcto");


        dto.setId(idTurno);
//        dto->entidad;
//        guardar;
//        entidad->dto;
        return
                turnoMapper.entityToEnteroDto(
                        turnoRepository.save(
                                turnoMapper.enteroDtoToEntity(
                                        dto
                                )
                        )
                );

    }

    public boolean eliminar(Integer id){
        if (!turnoRepository.existsById(id))
            throw new RuntimeException("No existe el Turno");
        turnoRepository.deleteById(id);
        return true;
    }

//////////////////////////////////////////////////////////////////////////////

    public Page<Turno> listarTodosByPage(Integer pageNumber, Integer pagSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pagSize, Sort.by(orderField));
        return turnoRepository.findAll(pageable);
    }

    public Page<TurnoDto> listaUnoByPage(int id ,Integer pageNumber, Integer pageSize) {
        Pageable pageable= PageRequest.of(pageNumber, pageSize);
        return turnoRepository.findById(id,pageable).map(turnoMapper::entityToDto);
    }
}
