package com.folcademy.clinica.Services;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPersonaService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("PersonaService")
public class PersonaService implements IPersonaService {
    private final PersonaRepository personaRepository;
    private final PersonaMapper personaMapper;

    public PersonaService(PersonaRepository personaRepository, PersonaMapper personaMapper) {
        this.personaRepository = personaRepository;
        this.personaMapper = personaMapper;
    }

    @Override
    public List<Persona> findAllPersonas() {
        List<Persona> personas = (List<Persona>) personaRepository.findAll();
        return personas;
    }

    public PersonaDto listarUno(Integer id){
        return personaRepository.findById(id).map(personaMapper::entityToDto).orElse(null);}

    public PersonaDto agregar(PersonaDto entity){
        entity.setIdpersona(null);

        return personaMapper.entityToDto(personaRepository.save(personaMapper.dtoToEntity(entity)));
    }

    public PersonaDto editar(Integer idPersona, PersonaDto dto){

        dto.setIdpersona(idPersona);
//        dto->entidad;
//        guardar;
//        entidad->dto;
        return
                personaMapper.entityToDto(
                        personaRepository.save(
                                personaMapper.dtoToEntity(
                                        dto
                                )
                        )
                );

    }
}
