package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.PacienteDto;


import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.PacienteMapper;

import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.PacienteRepository;

import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import com.folcademy.clinica.Services.Interfaces.IPacienteService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;


import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service("PacienteSerive")
public class PacienteService implements IPacienteService {
    private final PacienteRepository pacienteRepository;
    private final PacienteMapper pacienteMapper;
    private final PersonaMapper personaMapper;
    private final PersonaRepository personaRepository;
    private final PersonaService personaService;


    public PacienteService(PacienteRepository pacienteRepository, PacienteMapper pacienteMapper, PersonaMapper personaMapper, PersonaRepository personaRepository, PersonaService personaService)
    {
        this.pacienteRepository = pacienteRepository;
        this.pacienteMapper = pacienteMapper;
        this.personaMapper = personaMapper;
        this.personaRepository = personaRepository;
        this.personaService = personaService;
    }

    @Override
    public List<Paciente> findAllPacientes() {
        List<Paciente> pacientes = (List<Paciente>) pacienteRepository.findAll();
        return pacientes;
    }

    public List<PacienteEnteroDto> listarTodos(){
        List<Paciente> pacientes = (List<Paciente>) pacienteRepository.findAll();
        return pacientes.stream().map(pacienteMapper::entityToEnteroDto).collect(Collectors.toList());
    }

    public PacienteDto listarUno(Integer id){
        if (!pacienteRepository.existsById(id))
            throw new RuntimeException("No existe el paciente");

        return pacienteRepository.findById(id).map(pacienteMapper::entityToDto).orElse(null);
    }

    public PacienteEnteroDto agregar(PacienteEnteroDto dto){
        dto.setId(null);


        if (dto.getPersona() != null)
            if (dto.getPersona().getIdpersona() != null)
                if (personaRepository.existsById(dto.getPersona().getIdpersona()))
                    return pacienteMapper.entityToEnteroDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(dto, personaRepository.findById(dto.getPersona().getIdpersona()).get())));

        if (dto.getPersona() != null) {
            Persona persona = personaMapper.dtoToEntity(personaService.agregar(dto.getPersona()));
            return pacienteMapper.entityToEnteroDto(pacienteRepository.save(pacienteMapper.enteroDtoToEntity(dto, persona)));

        }


        throw new BadRequestException("persona vacia");
    }



     public PacienteEnteroDto editar(Integer idPaciente, PacienteEnteroDto dto){
        if(!pacienteRepository.existsById(idPaciente))
            throw new RuntimeException("No existe el paciente");

         if(personaRepository.existsById(dto.getPersona().getIdpersona()))
             if (pacienteRepository.existsById(idPaciente))
                 dto.setId(idPaciente);
         Persona persona  =personaMapper.dtoToEntity(personaService.editar(dto.getPersona().getIdpersona(),dto.getPersona()));
        dto.setId(idPaciente);
//        dto->entidad;
//        guardar;
//        entidad->dto;
        return
                pacienteMapper.entityToEnteroDto(
                        pacienteRepository.save(
                                pacienteMapper.enteroDtoToEntity(
                                 dto, persona
                                )
                        )
                );

    }


    public boolean eliminar(Integer id){
        if (!pacienteRepository.existsById(id))
            throw new RuntimeException("No existe el paciente");
        pacienteRepository.deleteById(id);
        return true;
    }

    ///////////////////////////////////////////////////////////////////////////////////////

    public Page<PacienteEnteroDto> listarTodosByPage(Integer pageNumber, Integer pagSize, String orderField) {
        Pageable pageable = PageRequest.of(pageNumber,pagSize, Sort.by(orderField));
        return pacienteRepository.findAll(pageable).map(pacienteMapper::entityToEnteroDto);
    }

    /*public Page<PacienteDto> listaUnoByPage(int id,Integer pageNumber,Integer pageSize)
    {
        Pageable pageable= PageRequest.of(pageNumber,pageSize);
        return pacienteRepository.findById(id,pageable).map(pacienteMapper::entityToDto);

    }*/

}
