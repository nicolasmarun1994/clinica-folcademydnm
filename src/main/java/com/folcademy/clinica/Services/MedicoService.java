package com.folcademy.clinica.Services;


import com.folcademy.clinica.Exceptions.BadRequestException;
import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;

import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Mappers.MedicoMapper;
import com.folcademy.clinica.Model.Mappers.PersonaMapper;
import com.folcademy.clinica.Model.Repositories.MedicoRespoistory;
import com.folcademy.clinica.Model.Repositories.PersonaRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


@Service("medicoService")
public class MedicoService {
    private final MedicoRespoistory medicoRepository;
    private final MedicoMapper medicoMapper; //Conectamos el Mapper
    private final PersonaMapper personaMapper;
    private final PersonaRepository personaRepository;
    private final PersonaService personaService;

    public MedicoService(MedicoRespoistory medicoRepository, MedicoMapper medicoMapper, PersonaMapper personaMapper, PersonaRepository personaRepository, PersonaService personaService) {
        this.medicoRepository = medicoRepository;
        this.medicoMapper = medicoMapper;
        this.personaMapper = personaMapper;
        this.personaRepository = personaRepository;
        this.personaService = personaService;
    }

    public List<MedicoEnteroDto> listarTodos() { //Tenemos las funciones conectadas al mapper ahora a traves del Dto

        List<Medico> medicos = (List<Medico>) medicoRepository.findAll();
        return medicos.stream().map(medicoMapper::entityToEnteroDto).collect(Collectors.toList());
    }

    public MedicoDto listarUno(Integer id) {
        if (!medicoRepository.existsById(id))
            throw new RuntimeException("No existe el medico");
        return medicoRepository.findById(id).map(medicoMapper::entityToDto).orElse(null);
    }

    public MedicoEnteroDto agregar(MedicoEnteroDto dto) {
        dto.setId(null);

        if (dto.getConsulta() < 0)
            throw new BadRequestException("La consulta no peude ser menor a 0");
        if (Objects.equals(dto.getProfesion(), ""))
            throw new BadRequestException("La profecion no puede estar vacia");

        if (dto.getPersona() != null)
            if (dto.getPersona().getIdpersona() != null)
                if (personaRepository.existsById(dto.getPersona().getIdpersona()))
                    return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(dto, personaRepository.findById(dto.getPersona().getIdpersona()).get())));

        if (dto.getPersona() != null) {
            Persona persona = personaMapper.dtoToEntity(personaService.agregar(dto.getPersona()));
            return medicoMapper.entityToEnteroDto(medicoRepository.save(medicoMapper.enteroDtoToEntity(dto, persona)));

        }


        throw new BadRequestException("persona vacia");


    }


    public MedicoEnteroDto editar(Integer idMedico, MedicoEnteroDto dto){
        if(!medicoRepository.existsById(idMedico))
            throw  new RuntimeException("No existe el medico");

        if (dto.getConsulta()<0)
            throw new BadRequestException("La consulta no peude ser menor a 0");
        if (dto.getProfesion()=="")
            throw new BadRequestException("La profecion no puede estar vacia");

        if(personaRepository.existsById(dto.getPersona().getIdpersona()))
            if (medicoRepository.existsById(idMedico))
            dto.setId(idMedico);
            Persona persona  =personaMapper.dtoToEntity(personaService.editar(dto.getPersona().getIdpersona(),dto.getPersona()));

    //        dto->entidad;
    //        guardar;
    //        entidad->dto;
            return
                    medicoMapper.entityToEnteroDto(
                            medicoRepository.save(
                                    medicoMapper.enteroDtoToEntity(
                                            dto, persona
                                    )
                            )
                    );

        }

    public boolean eliminar(Integer id) {
        if (!medicoRepository.existsById(id))
            throw new RuntimeException("No existe el medico");
        medicoRepository.deleteById(id);
        return true;
    }
//////////////////////////////////////////////////////////////////////////////////

    public Page<MedicoEnteroDto> listarTodosByPage(Integer pageNumber, Integer pagSize)
    {
        Pageable pageable = PageRequest.of(pageNumber, pagSize);
        return medicoRepository.findAll(pageable).map(medicoMapper::entityToEnteroDto);
    }


    public Page<MedicoDto> listaUnoByPage(int id ,Integer pageNumber, Integer pageSize) {
        Pageable pageable= PageRequest.of(pageNumber, pageSize);
        return medicoRepository.findById(id,pageable).map(medicoMapper::entityToDto);
    }



}
