package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.PersonaService;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
/*
@RestController
@RequestMapping ("/personas")
public class PersonaController {
    private final PersonaService personaService;

    public PersonaController(PersonaService personaService) {
        this.personaService = personaService;
    }


    @GetMapping(value = "")
    public ResponseEntity<List<Persona>> findAll() {
        return ResponseEntity.ok().body(personaService.findAllPersonas());
    }

    @GetMapping("/{idPersona}") //Nos devuelve solo 1 medico
    public  ResponseEntity<PersonaDto> listarUno(@PathVariable(name = "idPersona") int id) {
        return ResponseEntity.ok(personaService.listarUno(id));
    }

    @PostMapping("")
    public ResponseEntity<PersonaDto> agregar(@RequestBody @Validated PersonaDto entity){
        return ResponseEntity.ok(personaService.agregar(entity));
    }

    @PutMapping("/{idPersona}")
    public ResponseEntity<PersonaDto> editar(@PathVariable(name = "idPaciente")int id,
                                                    @RequestBody PersonaDto dto){
        return ResponseEntity.ok(personaService.editar(id,dto));
    }

}
*/
