package com.folcademy.clinica.Controllers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;

import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Services.MedicoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/medicos")
public class MedicoController {
    private final MedicoService medicoService;

    public MedicoController(MedicoService medicoService) {
        this.medicoService = medicoService;
    }


    @GetMapping("")
    public ResponseEntity<List<MedicoEnteroDto>>listarTodo(){
        return ResponseEntity.ok(medicoService.listarTodos());
    }


    @GetMapping("/page")
    public ResponseEntity<Page<MedicoEnteroDto>>listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pagSize
            //@RequestParam(name = "orderField", defaultValue = "idpersona") String orderField

    ){
        return ResponseEntity.ok(medicoService.listarTodosByPage(pageNumber,pagSize));
    }


    /*@GetMapping("/{idMedico}")
    public  ResponseEntity<MedicoDto> listarUno(@PathVariable(name = "idMedico") int id){
        return ResponseEntity.ok(medicoService.listarUno(id));
    }*/
    @GetMapping("/{idMedico}")
    public  ResponseEntity<Page<MedicoDto>>listarUno(
            @PathVariable(name="idMedico")int id,
            @RequestParam(name = "pageNumber",defaultValue ="0")Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue ="1")Integer pageSize,
            @RequestParam(name = "orderField",defaultValue ="apellido")String orderfield)
    {
        return  ResponseEntity.ok(medicoService.listaUnoByPage(id,pageNumber,pageSize));
    }


    @PostMapping("")
    public ResponseEntity<MedicoEnteroDto> agregar(@RequestBody MedicoEnteroDto dto){
        return ResponseEntity.ok(medicoService.agregar(dto));
    }


    @PutMapping("/{idMedico}")
    public ResponseEntity<MedicoEnteroDto> editar(@PathVariable(name = "idMedico")int id,
                                                    @RequestBody  MedicoEnteroDto dto){
        return ResponseEntity.ok(medicoService.editar(id,dto));
    }


    @DeleteMapping("/{idMedico}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idMedico")int id)
                                                                {
        return ResponseEntity.ok(medicoService.eliminar(id));
    }


}
