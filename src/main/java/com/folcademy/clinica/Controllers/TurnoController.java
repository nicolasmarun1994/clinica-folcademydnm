
package com.folcademy.clinica.Controllers;


import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import com.folcademy.clinica.Services.TurnoService;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping ("/turnos")
public class TurnoController {

    private final TurnoService turnoService;

    public TurnoController(TurnoService turnoService) {
        this.turnoService = turnoService;
    }



    @GetMapping(value = "")
    public ResponseEntity<List<Turno>>findAll() {
        return ResponseEntity.ok().body(turnoService.findAllTurnos());
    }


    @GetMapping("/page")
    public ResponseEntity<Page<Turno>>listarTodoByPage(
            @RequestParam(name = "pageNumber", defaultValue = "1") Integer pageNumber,
            @RequestParam(name = "pageSize", defaultValue = "2") Integer pagSize,
            @RequestParam(name = "orderField", defaultValue = "idPaciente") String orderField

    ){
        return ResponseEntity.ok(turnoService.listarTodosByPage(pageNumber,pagSize,orderField));
    }


    /*
    @GetMapping("/{idTurno}")
    public  ResponseEntity<TurnoEnteroDto> listarUno(@PathVariable(name = "idTurno") int id){
        return ResponseEntity.ok(turnoService.listarUno(id));
    }*/

    @GetMapping("/{idTurno}")
    public  ResponseEntity<Page<TurnoDto>>listarUno(
            @PathVariable(name="idTurno")int id,
            @RequestParam(name = "pageNumber",defaultValue ="0")Integer pageNumber,
            @RequestParam(name = "pageSize",defaultValue ="1")Integer pageSize,
            @RequestParam(name = "orderField",defaultValue ="fecha") Date orderfield)
    {
        return  ResponseEntity.ok(turnoService.listaUnoByPage(id,pageNumber,pageSize));
    }


    @PostMapping("")
    public ResponseEntity<TurnoEnteroDto> agregar(@RequestBody @Validated TurnoEnteroDto entity){
        return ResponseEntity.ok(turnoService.agregar(entity));
    }


    @PutMapping("/{idTurno}")
    public ResponseEntity<TurnoEnteroDto> editar(@PathVariable(name = "idTurno")int id,
                                                  @RequestBody  TurnoEnteroDto dto){
        return ResponseEntity.ok(turnoService.editar(id,dto));
    }


    @DeleteMapping("/{idTurno}")
    public ResponseEntity<Boolean> eliminar(@PathVariable(name = "idTurno")int id)
    {
        return ResponseEntity.ok(turnoService.eliminar(id));
    }



}
