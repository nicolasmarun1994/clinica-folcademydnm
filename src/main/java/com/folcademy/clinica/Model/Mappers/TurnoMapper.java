
package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.TurnoDto;
import com.folcademy.clinica.Model.Dtos.TurnoEnteroDto;
import com.folcademy.clinica.Model.Entities.Turno;
import org.springframework.stereotype.Component;


import java.util.Optional;

@Component
public class TurnoMapper {
    private final PacienteMapper pacienteMapper;
    private final MedicoMapper medicoMapper;

    public TurnoMapper(PacienteMapper pacienteMapper, MedicoMapper medicoMapper) {
        this.pacienteMapper = pacienteMapper;
        this.medicoMapper = medicoMapper;
    }

    public TurnoDto entityToDto(Turno entity)
    {
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdPaciente(),
                                ent.getIdMedico()
                        )
                )
                .orElse((new TurnoDto()));
    }

    public Turno dtoToEntity(TurnoDto dto)
    {
        Turno entity = new Turno();
        entity.setId((dto.getId()));
        entity.setFecha(dto.getFecha());
        entity.setHora((dto.getHora()));
        return entity;
    }

    public TurnoEnteroDto entityToEnteroDto(Turno entity){

        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new TurnoEnteroDto(
                                ent.getId(),
                                ent.getFecha(),
                                ent.getHora(),
                                ent.getAtendido(),
                                ent.getIdPaciente(),
                                ent.getIdMedico()



                        )
                )
                .orElse(new TurnoEnteroDto());
    }

    public Turno enteroDtoToEntity(TurnoEnteroDto dto){
        Turno entity = new Turno();
        entity.setId(dto.getId());
        entity.setFecha(dto.getFecha());
        entity.setHora(dto.getHora());
        entity.setAtendido(dto.getAtendido());
        entity.setIdPaciente(dto.getIdpaciente());
        entity.setIdMedico(dto.getIdmedico());


        return entity;
    }



}
