package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.MedicoDto;
import com.folcademy.clinica.Model.Dtos.MedicoEnteroDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Medico;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Services.PersonaService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class MedicoMapper {

    private final PersonaService personaService;
    private final PersonaMapper personaMapper;

    public MedicoMapper(PersonaService personaService, PersonaMapper personaMapper) {
        this.personaService = personaService;

        this.personaMapper = personaMapper;
    }


    public MedicoDto entityToDto(Medico entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new MedicoDto(
                                ent.getId(),
                                    /*ent.getNombre(),
                                    ent.getApellido(),*/
                                ent.getProfesion(),
                                ent.getConsulta(),
                                ent.getIdpersona(),
                                personaService.listarUno(ent.getIdpersona())

                        )
                )
                .orElse(new MedicoDto());
    }

    public Medico dtoToEntity(MedicoDto dto){
        Medico entity = new Medico();
        entity.setId(dto.getId());
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersona(dto.getIdpersona());
        return entity;
    }



    public MedicoEnteroDto entityToEnteroDto(Medico entity){


            return Optional
                    .ofNullable(entity)
                    .map(
                            ent -> new MedicoEnteroDto(
                                    ent.getId(),
                                    /*ent.getNombre(),
                                    ent.getApellido(),*/
                                    ent.getProfesion(),
                                    ent.getConsulta(),
                                    ent.getIdpersona(),

                                    personaService.listarUno(ent.getIdpersona())

                            )
                    )
                    .orElse(new MedicoEnteroDto());


    }

    public Medico enteroDtoToEntity(MedicoEnteroDto dto, Persona persona){
        Medico entity = new Medico();
        entity.setId(dto.getId());
        /*entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());*/
        entity.setProfesion(dto.getProfesion());
        entity.setConsulta(dto.getConsulta());
        entity.setIdpersona(persona.getIdpersona());

        return entity;
    }


}
