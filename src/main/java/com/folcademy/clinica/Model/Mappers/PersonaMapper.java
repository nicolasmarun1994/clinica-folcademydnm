package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PersonaDto;
import com.folcademy.clinica.Model.Entities.Persona;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PersonaMapper {

    public PersonaDto entityToDto(Persona entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PersonaDto(
                                ent.getIdpersona(),
                                ent.getDni(),
                                ent.getNombre(),
                                ent.getApellido(),
                                ent.getTelefono()
                                //ent.getDireccion()
                        )
                )
                .orElse(new PersonaDto());
    }

    public  Persona dtoToEntity(PersonaDto dto){


        Persona entity = new Persona();
        entity.setIdpersona(dto.getIdpersona());
        entity.setDni(dto.getDni());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());
        entity.setTelefono(dto.getTelefono());
        //entity.setDireccion(dto.getDireccion());

        return entity;
    }
}
