package com.folcademy.clinica.Model.Mappers;

import com.folcademy.clinica.Model.Dtos.PacienteDto;
import com.folcademy.clinica.Model.Dtos.PacienteEnteroDto;

import com.folcademy.clinica.Model.Entities.Paciente;
import com.folcademy.clinica.Model.Entities.Persona;
import com.folcademy.clinica.Services.PersonaService;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PacienteMapper {
    private final PersonaService personaService;

    public PacienteMapper(PersonaService personaService) {
        this.personaService = personaService;
    }

    public PacienteDto entityToDto(Paciente entity){
        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteDto(
                                ent.getIdpaciente(),
                                ent.getIdpersona(),
                                ent.getDireccion(),
                                personaService.listarUno(ent.getIdpersona())

                        )
                )
                .orElse(new PacienteDto());
    }

    public Paciente dtoToEntity(PacienteDto dto){
        Paciente entity = new Paciente();
        entity.setIdpaciente(dto.getId());
        entity.setIdpersona(dto.getIdpersona());



        return entity;
    }

    public PacienteEnteroDto entityToEnteroDto(Paciente entity){


        return Optional
                .ofNullable(entity)
                .map(
                        ent -> new PacienteEnteroDto(
                                ent.getIdpaciente(),
                               /* ent.getDni(),
                                ent.getNombre(),
                                ent.getApellido(),

                                ent.getTelefono(),*/
                                ent.getIdpersona(),
                                ent.getDireccion(),
                                personaService.listarUno(ent.getIdpersona())

                        )
                )
                .orElse(new PacienteEnteroDto());


    }

    public Paciente enteroDtoToEntity(PacienteEnteroDto dto, Persona persona){
        Paciente entity = new Paciente();
        entity.setIdpaciente(dto.getId());
       /* entity.setDni(dto.getDni());
        entity.setNombre(dto.getNombre());
        entity.setApellido(dto.getApellido());

        entity.setTelefono(dto.getTelefono());*/
        entity.setIdpersona(persona.getIdpersona());
        entity.setDireccion(dto.getDireccion());
        return entity;
    }



}
