package com.folcademy.clinica.Model.Dtos;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MedicoEnteroDto {
    Integer id;
    /*String nombre = "";
    String apellido = "";*/
    String profesion = "";
    int consulta = 0;
    public Integer idpersona;
    PersonaDto persona;
}
