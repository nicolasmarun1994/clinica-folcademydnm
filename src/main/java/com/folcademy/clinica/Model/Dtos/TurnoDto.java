
package com.folcademy.clinica.Model.Dtos;

import com.sun.istack.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.time.LocalDate;
import java.time.LocalTime;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class TurnoDto {
    Integer id;

    @NotNull
    LocalDate fecha;
    @NotNull
    LocalTime hora;
    Boolean atendido;
    Integer idpaciente;
    Integer idmedico;
}
