package com.folcademy.clinica.Model.Entities;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name = "medico")

@Data
@RequiredArgsConstructor
public class Medico  {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idmedico", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;

   /* @Column(name = "nombre" , columnDefinition = "VARCHAR")
    public String nombre = ""; // si le asignamos vacio, nos sirve para el Dto, no tener que asignarles nada.

    @Column(name = "apellido" , columnDefinition = "VARCHAR")
    public String apellido = "";*/

    @Column(name = "profesion", columnDefinition = "VARCHAR")
    public String profesion = "";

    @Column(name = "consulta")
    public int consulta = 0; //con el int podemos arrancar en 0, con Integer puede ser cualquier valor

    @Column(name = "idpersona", columnDefinition = "INT")
    public Integer idpersona;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpersona",referencedColumnName = "idpersona", insertable = false, updatable = false)
    private Persona persona;
}
