package com.folcademy.clinica.Model.Entities;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;
import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Data
@Table(name = "turno")

public class Turno {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idturno", columnDefinition = "INT(10) UNSIGNED")
    public Integer id;

    @Column(name = "fecha", columnDefinition = "DATE")
    public LocalDate fecha;

    @Column(name = "hora", columnDefinition = "TIME")
    public LocalTime hora;

    @Column(name = "atendido", columnDefinition = "TINYINT")
    public Boolean atendido;


    @Column(name = "idpaciente", columnDefinition = "INT")
    public Integer idPaciente;


    @Column(name = "idmedico", columnDefinition = "INT")
    public Integer idMedico;



    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpaciente",referencedColumnName = "idpaciente", insertable = false, updatable = false)
    private Paciente paciente;

    @ManyToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idmedico",referencedColumnName = "idmedico", insertable = false, updatable = false)
    private Medico medico;



}
