package com.folcademy.clinica.Model.Entities;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;

@Entity
@Table(name = "paciente")
@Data
@RequiredArgsConstructor
public class Paciente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idpaciente", columnDefinition = "INT(10) UNSIGNED")
    public Integer idpaciente;
    /*@Column(name = "dni", columnDefinition = "VARCHAR")
    public String dni = "";
    @Column(name = "Nombre", columnDefinition = "VARCHAR")
    public String nombre = "";
    @Column(name = "Apellido", columnDefinition = "VARCHAR")
    public String apellido = "";
    @Column(name = "Telefono", columnDefinition = "VARCHAR")
    public String telefono ="";*/

    @Column(name = "idpersona", columnDefinition = "INT")
    public Integer idpersona;
    @Column(name = "direccion", columnDefinition = "VARCHAR")
    public String direccion ="";

    @OneToOne
    @NotFound(action = NotFoundAction.IGNORE)
    @JoinColumn(name = "idpersona",referencedColumnName = "idpersona", insertable = false, updatable = false)
    private Persona persona;

}
